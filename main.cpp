#include "./lib/json.hpp"
#include <iostream>

using json = nlohmann::json;
using namespace std;

template<class UnaryFunction>

void check(const json& j, UnaryFunction f)
{
    for(auto it = j.begin(); it != j.end(); ++it)
    {
        if (it->is_structured())
        {
            check(*it, f);
        }
        else
        {
            cout<<it.value()<<endl;
            f(it);
        }
    }
}

int main()
{
    json  j = {
          {"name", "Rogério de Oliveira Batista"},
  {"phone", "(38)99809-8256"},
  {"address", {
    {"street", "Rua dos Tamóios, 45"},
    {"neighborhood", "Centro"},
    {"state", { {"stateName", "Minas Gerais"}, {"stateCode", 31 }}},
    {"city", { {"cityName", "Pouso Alegre"}, {"cityCode", 19} }}
  }}
    };

    check(j, [](json::const_iterator it){
        // std::cout << *it << std::endl;
    });
    return 0;
}